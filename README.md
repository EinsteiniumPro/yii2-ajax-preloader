###Yii 2 extension that displays an animation during Ajax request.

Installation
------------
#### Run command
```
composer require xalberteinsteinx/yii2-ajax-preloader
```
or add
```
"xalberteinsteinx/yii2-ajax-preloader": "*"
```
to the require section of your composer.json.