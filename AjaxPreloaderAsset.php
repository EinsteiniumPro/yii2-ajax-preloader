<?php
namespace einsteinium\ajaxpreloader;
use yii\web\AssetBundle;

/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */
class AjaxPreloaderAsset extends AssetBundle
{
    public $sourcePath = '@vendor/einsteinium/yii2-ajax-preloader';


    public $css = [
        'css/ajax-preloader.css'
    ];

    public $js = [
        'js/ajax-preloader.js'
    ];

    public $depends = [
        'yii\widgets\PjaxAsset'
    ];
}